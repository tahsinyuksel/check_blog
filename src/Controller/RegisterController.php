<?php


namespace App\Controller;


use App\Entity\User;
use App\Form\Type\RegisterFormType;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisterController extends AbstractController
{
    /**
     * @Route("/register", name="app_register")
     */
    public function register(UserPasswordEncoderInterface $passwordEncoder, Request $request, UserService $userService): Response
    {
        $user = new User();
        $form = $this->createForm(RegisterFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                /** @var User $user */
                $user = $form->getData();
                $user->setPassword($passwordEncoder->encodePassword(
                    $user,
                    (string)$form->get('password')->getData()
                ));
                if ($userService->save($user)) {
                    $this->addFlash('success', 'Register Success');
                    return $this->redirectToRoute('post_index');
                }
            }
        }

        return $this->render('security/register.html.twig', [
            'form'=> $form->createView()
        ]);
    }
}
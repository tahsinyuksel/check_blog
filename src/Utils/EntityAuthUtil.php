<?php


namespace App\Utils;


use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class EntityAuthUtil
{

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * EntityAuthUtil constructor.
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        $user = $this->tokenStorage->getToken() && $this->tokenStorage->getToken()->getUser();
        return is_object($user) ? $user : null;
    }

    public function getEntityAuthUser($currentUser, $entity, $field): bool
    {
        $currentUser = $currentUser == null ? $this->getUser() : $currentUser;
        if (!$currentUser || !$entity) {
            return false;
        }

        $entityUser = $this->getEntityUser($entity, $field);

        if (!$entityUser) {
            return false;
        }

        if ((int)$entityUser->getId() != (int)$currentUser->getId()) {
            return false;
        }
        return true;
    }


    /**
     * @param object $entity
     * @param string $field
     * @return object|null
     */
    public function getEntityUser($entity, string $field)
    {
        $entityUser = null;
        if (empty($field)) {
            $entityUser = $entity;
        } else if (method_exists($entity, $field)) {
            $method = $field;
            $entityUser = $entity->$method();
        }

        return $entityUser;
    }
}
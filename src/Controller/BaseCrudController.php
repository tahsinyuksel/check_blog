<?php


namespace App\Controller;


use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class BaseCrudController extends BaseController
{
    const MAX_PAGE_LIMIT = 10;

    public function index(Request $request): Response
    {
        $query = $this->entityService->queryBuilder->getQuery();
        $currentPage = $request->query->get('page', 1);
        $limit = $request->query->get('limit', self::MAX_PAGE_LIMIT);
        $limit = $limit > self::MAX_PAGE_LIMIT ? self::MAX_PAGE_LIMIT : $limit;
        $paginator = new Paginator($query);
        $paginator
            ->getQuery()
            ->setFirstResult($limit * ($currentPage - 1))
            ->setMaxResults($limit);

        $list = $paginator->getQuery()->getResult();
        return $this->crudView('index', [
            'list' => $list,
            'lastPage' => ceil($paginator->count() / $paginator->getQuery()->getMaxResults()),
            'currentPage' => $currentPage,
            'nextPage' => $paginator->count() > 0 ? $currentPage + 1 : 1,
            'prevPage' => $currentPage > 1 ? $currentPage - 1 : 1,
            'limit' => $limit,
        ]);
    }

    /**
     * @throws \Exception
     */
    public function detail(int $id): Response
    {
        $entity = $this->getEntityForRequest($id);
        $this->handleAuthEntity('detail', $entity, null);

        return $this->crudView('detail', [
            'entity' => $entity,
        ], $id);
    }

    /**
     * @throws \Exception
     */
    public function create(Request $request): Response
    {
        return $this->edit(0, $request);
    }

    /**
     * @throws \Exception
     */
    public function edit(int $id, Request $request): Response
    {
        $entity = $this->getEntityForRequest($id);
        $this->handleAuthEntity($id > 0 ? 'edit' : 'create', $entity, $request);
        $form = $this->createForm($this->getCreateFormType(), $entity);
        $result = $this->formSave($form, $request, 'Success!', 'Error!');
        if ($result->isProcess()) {
            if ($result->isSuccess()) {
                return $this->redirect($this->getCrudLinks($this->getEntityName(), $form->getData()->getId())['detailLink']);
            }
        }

        return $this->crudView('create', [
            'form' => $form->createView(),
            'entity' => $entity,
        ], $id);
    }

    /**
     * @throws \Exception
     */
    public function delete(int $id): Response
    {
        $entity = $this->getEntityForRequest($id);
        $this->handleAuthEntity('delete', $entity, null);

        if ($this->isSoftDelete()) {
            $result = $this->entityService->softDelete($entity);
        } else {
            $result = $this->entityService->remove($entity);
        }

        $this->customResultAction($result, 'Removed', 'Not Removed');
        if ($result) {
            return $this->redirect($this->getCrudLinks($this->getEntityName(), $id)['indexLink']);
        }

        return $this->redirect($this->getCrudLinks($this->getEntityName(), $id)['detailLink']);
    }
}
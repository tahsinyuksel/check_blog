<?php


namespace App\Service;


use Doctrine\ORM\EntityManagerInterface;

abstract class BaseEntityService
{
    protected $em;
    protected $entity;
    protected $entityClass;
    protected $repository;
    public $queryBuilder;

    public function __construct(EntityManagerInterface $em, $entityClass)
    {
        $this->em = $em;
        $this->entityClass = $entityClass;
        $this->repository = $em->getRepository($entityClass);
        $this->queryBuilder = $em->getRepository($entityClass)->createQueryBuilder('e');
    }

    /**
     * @throws \Exception
     */
    public function create()
    {
        return new $this->entityClass();
    }

    public function save($entity): bool
    {
        if(!empty($entity))
        {
            $this->em->persist($entity);
            $this->em->flush();
            return true;
        }
        return false;
    }

    public function remove($entity): bool
    {
        $this->em->remove($entity);
        $this->em->flush();
        return true;
    }

    public function softDelete($entity): bool
    {
        if (method_exists($entity, 'setDeleted')) {
            $entity->setDeleted(true);
            return $this->save($entity);
        }
        return false;
    }

    /**
     * @return \Doctrine\Persistence\ObjectRepository
     */
    public function getRepository(): \Doctrine\Persistence\ObjectRepository
    {
        return $this->repository;
    }



}
<?php


namespace App\Form\Type;


use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class RegisterFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("firstname", TextType::class, ['attr' => ['placeholder' => 'Firstname']])
            ->add("lastname", TextType::class, ['attr' => ['placeholder' => 'Lastname']])
            ->add("email", TextType::class, ['attr' => ['placeholder' => 'email']])
            ->add("password", PasswordType::class, ['attr' => ['placeholder' => 'password']])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'=> User::class
        ]);
    }

    public function getName(): string
    {
        return 'user_register';
    }

}
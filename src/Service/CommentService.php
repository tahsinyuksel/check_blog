<?php


namespace App\Service;


use App\Entity\Comment;
use Doctrine\ORM\EntityManagerInterface;

class CommentService extends BaseEntityService
{
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em, Comment::class);
    }

}
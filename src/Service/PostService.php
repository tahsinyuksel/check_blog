<?php


namespace App\Service;


use App\Entity\Post;
use Doctrine\ORM\EntityManagerInterface;

class PostService extends BaseEntityService
{
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em, Post::class);
    }

}
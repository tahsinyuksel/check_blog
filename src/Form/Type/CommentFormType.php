<?php


namespace App\Form\Type;


use App\Entity\Comment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CommentFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("content", TextType::class, ['attr' => ['placeholder' => 'content']])
            /*
            ->add("createdAt", DateTimeType::class, ['attr' => ['placeholder' => 'createdAt']])
            ->add("user", EntityType::class, [
                'attr' => ['class' => 'form-control'],
                'class' => User::class,
            ])
            */
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'=> Comment::class
        ]);
    }

    public function getName(): string
    {
        return 'comment_form';
    }

}
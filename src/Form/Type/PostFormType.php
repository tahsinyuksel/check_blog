<?php


namespace App\Form\Type;


use App\Entity\Post;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class PostFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("title", TextType::class, ['attr' => ['placeholder' => 'title']])
            ->add("content", TextType::class, ['attr' => ['placeholder' => 'content']])
            ->add("showHome", CheckboxType::class, ['required'=> false, 'label'=> 'is show home'])
            /*
            ->add("createdAt", DateTimeType::class, ['attr' => ['placeholder' => 'createdAt']])
            ->add("user", EntityType::class, [
                'attr' => ['class' => 'form-control'],
                'class' => User::class,
            ])
            */
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'=> Post::class
        ]);
    }

    public function getName(): string
    {
        return 'post_form';
    }

}
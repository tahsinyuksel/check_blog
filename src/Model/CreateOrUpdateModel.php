<?php


namespace App\Model;


use Symfony\Component\HttpFoundation\RedirectResponse;

class CreateOrUpdateModel
{
    /**
     * @var bool
     */
    protected $isProcess = false;

    /**
     * @var bool
     */
    protected $success = false;

    /**
     * @var string
     */
    protected $successMessage = '';

    /**
     * @var string
     */
    protected $errorMessage = '';

    /**
     * CreateOrUpdateModel constructor.
     */
    public function __construct()
    {
        $this->isProcess = false;
        $this->success = false;
    }


    /**
     * @return string
     */
    public function getSuccessMessage(): string
    {
        return $this->successMessage;
    }

    /**
     * @param string $successMessage
     * @return $this
     */
    public function setSuccessMessage(string $successMessage): CreateOrUpdateModel
    {
        $this->successMessage = $successMessage;
        return $this;
    }

    /**
     * @return string
     */
    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    /**
     * @param string $errorMessage
     * @return $this
     */
    public function setErrorMessage(string $errorMessage): CreateOrUpdateModel
    {
        $this->errorMessage = $errorMessage;
        return $this;
    }

    /**
     * @return bool
     */
    public function isProcess(): bool
    {
        return $this->isProcess;
    }

    /**
     * @param bool $isProcess
     * @return $this
     */
    public function setIsProcess(bool $isProcess): CreateOrUpdateModel
    {
        $this->isProcess = $isProcess;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param bool $success
     * @return $this
     */
    public function setSuccess(bool $success): CreateOrUpdateModel
    {
        $this->success = $success;
        return $this;
    }
}
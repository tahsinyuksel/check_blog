<?php


namespace App\DataFixtures;


use App\Entity\Post;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class BlogFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $post = $this->postFixtures();
        $manager->persist($post);

        $user = $this->userFixtures();
        $manager->persist($user);

        $manager->flush();
    }

    private function postFixtures(): Post
    {
        $entity = new Post();
        $entity
            ->setTitle("post 1")
            ->setContent("post 1 content")
            ->setCreatedAt(new \DateTime())
        ;

        return $entity;
    }

    private function userFixtures(): User
    {
        $entity = new User();
        $entity
            ->setFirstname("ali")
            ->setLastname("cam")
            ->setEmail("alicam@check_blog.com")
            ->setPassword("1234")
        ;

        return $entity;
    }

}
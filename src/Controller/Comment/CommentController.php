<?php


namespace App\Controller\Comment;


use App\Controller\BaseCrudController;
use App\Entity\Comment;
use App\Entity\Post;
use App\Form\Type\CommentFormType;
use App\Service\CommentService;
use App\Service\PostService;
use App\Utils\EntityAuthUtil;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;

class CommentController extends BaseCrudController
{
    /**
     * @var PostService
     */
    protected $postService;

    /**
     * @var EntityAuthUtil
     */
    protected $entityAuthUtil;

    /**
     * PostController constructor.
     */
    public function __construct(CommentService $commentService, PostService $postService, EntityAuthUtil $entityAuthUtil)
    {
        $this->entityAuthUtil = $entityAuthUtil;
        $this->postService = $postService;
        parent::__construct($commentService, 'comment', CommentFormType::class, true);
    }

    public function handleFormEntity($formEntity, Request $request)
    {
        $postId = $request->query->get('postId', 0);
        if (0 == $postId) {
            return $formEntity;
        }

        if (! $formEntity instanceof Comment) {
            return $formEntity;
        }

        /** @var Post|null $post */
        $post = $this->postService->getRepository()->find($postId);
        if (!$post) {
            return $formEntity;
        }

        $formEntity->setPost($post);
        return $formEntity;
    }

    public function handleAuthEntity($name, &$formEntity, Request $request = null)
    {
        if (in_array($name, ['edit'])) {
            if (!$this->entityAuthUtil->getEntityAuthUser($this->getUser(), $formEntity, 'getUser')) {
                throw new AuthenticationCredentialsNotFoundException();
            }
        }
    }

}
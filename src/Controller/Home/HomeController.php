<?php


namespace App\Controller\Home;


use App\Service\PostService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(PostService $postService): Response
    {
        $list = $postService->getRepository()->findBy(['showHome'=> true], ['createdAt'=> 'desc'], 10);
        return $this->render('home/index.html.twig', [
            'list' => $list,
        ]);
    }
}
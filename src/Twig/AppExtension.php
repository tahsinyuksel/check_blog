<?php


namespace App\Twig;


use App\Utils\EntityAuthUtil;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    /**
     * @var EntityAuthUtil
     */
    protected $entityAuthUtil;

    /**
     * AppExtension constructor.
     * @param EntityAuthUtil $entityAuthUtil
     */
    public function __construct(EntityAuthUtil $entityAuthUtil)
    {
        $this->entityAuthUtil = $entityAuthUtil;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('hasEntityAuthUser', [$this->entityAuthUtil, 'getEntityAuthUser']),
        ];
    }

}
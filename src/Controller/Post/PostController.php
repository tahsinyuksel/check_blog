<?php


namespace App\Controller\Post;

use App\Controller\BaseCrudController;
use App\Form\Type\PostFormType;
use App\Service\PostService;
use App\Utils\EntityAuthUtil;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;

class PostController extends BaseCrudController
{
    /**
     * @var EntityAuthUtil
     */
    protected $entityAuthUtil;

    /**
     * PostController constructor.
     */
    public function __construct(PostService $postService, EntityAuthUtil $entityAuthUtil)
    {
        $this->entityAuthUtil = $entityAuthUtil;
        parent::__construct($postService, 'post', PostFormType::class, true);
    }

    public function handleAuthEntity($name, &$formEntity, Request $request = null)
    {
        if (in_array($name, ['edit'])) {
            if (!$this->entityAuthUtil->getEntityAuthUser($this->getUser(), $formEntity, 'getUser')) {
                throw new AuthenticationCredentialsNotFoundException();
            }
        }
    }


}
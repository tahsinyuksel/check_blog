<?php


namespace App\Form\Type;


use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class UserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("firstname", TextType::class, ['attr' => ['placeholder' => 'Firstname']])
            ->add("lastname", TextType::class, ['attr' => ['placeholder' => 'Lastname']])
            ->add("email", TextType::class, ['attr' => ['placeholder' => 'email']])
            /*
            ->add("createdAt", DateTimeType::class, [
                'attr' => ['class' => 'form-control'],
                'widget' => 'choice',
                'years' => range(date('Y')-90, date('Y')+90)
            ])
            */
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'=> User::class
        ]);
    }

    public function getName(): string
    {
        return 'user_form';
    }

}
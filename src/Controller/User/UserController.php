<?php


namespace App\Controller\User;

use App\Controller\BaseCrudController;
use App\Form\Type\UserFormType;
use App\Service\UserService;
use App\Utils\EntityAuthUtil;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;

class UserController extends BaseCrudController
{
    /**
     * @var EntityAuthUtil
     */
    protected $entityAuthUtil;

    /**
     * UserController constructor.
     */
    public function __construct(UserService $userService, EntityAuthUtil $entityAuthUtil)
    {
        $this->entityAuthUtil = $entityAuthUtil;
        parent::__construct($userService, 'user', UserFormType::class, false);
    }

    public function handleAuthEntity($name, &$formEntity, Request $request = null)
    {
        if (in_array($name, ['edit'])) {
            if (!$this->entityAuthUtil->getEntityAuthUser($this->getUser(), $formEntity, '')) {
                throw new AuthenticationCredentialsNotFoundException();
            }
        }
    }
}
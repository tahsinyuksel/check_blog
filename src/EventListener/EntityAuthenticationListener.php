<?php


namespace App\EventListener;

use App\Entity\Comment;
use App\Entity\Post;
use App\Entity\User;
use App\Utils\EntityAuthUtil;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;

class EntityAuthenticationListener
{

    /** @var ContainerInterface */
    protected $container;

    protected $flashBag;

    /**
     * @var EntityAuthUtil
     */
    protected $entityAuthUtil;

    /**
     * @var \Stringable|\Symfony\Component\Security\Core\User\UserInterface|null
     */
    protected $user = null;

    /**
     * @var array
     */
    protected $checkAuthEntityList = [
        ['class'=> Post::class, 'field'=> 'getUser'],
        ['class'=> User::class, 'field'=> ''],
    ];

    protected $checkAuthorEntityList = [
        ['class'=> Post::class, 'field'=> 'setUser'],
        ['class'=> Comment::class, 'field'=> 'setUser'],
    ];

    /**
     * EntityAuthenticationListener constructor.
     */
    public function __construct(ContainerInterface $container, TokenStorageInterface $tokenStorage, FlashBagInterface $flashBag, EntityAuthUtil $entityAuthUtil)
    {
        $this->container = $container;
        $this->flashBag = $flashBag;
        $user = $tokenStorage->getToken()->getUser();
        $this->user = is_object($user) ? $user : null;
        $this->entityAuthUtil = $entityAuthUtil;
    }

    public function preUpdate(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();
        $this->checkAuthEntity($entity);
        $this->checkAuthorEntity($entity);
    }

    public function postUpdate(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();
        $this->checkAuthEntity($entity);
        $this->checkAuthorEntity($entity);
    }

    public function preRemove(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();
        $this->checkAuthEntity($entity);
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();
        $this->checkAuthorEntity($entity);
    }

    /**
     * @param object $entity
     * @param array $checkList
     * @return array|null
     */
    private function hasCheckEntity($entity, array $checkList):?array
    {
        $hasCheckEntity = null;
        foreach ($checkList as $item) {
            if (get_class($entity) == $item['class']) {
                $hasCheckEntity = $item;
                break;
            }
        }

        return $hasCheckEntity;
    }

    /**
     * @param object $entity
     */
    private function checkAuthorEntity($entity)
    {
        $hasCheckEntity = $this->hasCheckEntity($entity, $this->checkAuthorEntityList);
        if (null == $hasCheckEntity) {
            return;
        }

        if (!method_exists($entity, $hasCheckEntity['field'])) {
            return;
        }

        if (!$this->getUser()) {
            return;
        }

        $method = $hasCheckEntity['field'];
        $entity->$method($this->getUser());
    }

    /**
     * @param object $entity
     */
    private function checkAuthEntity($entity)
    {
        $hasCheckEntity = $this->hasCheckEntity($entity, $this->checkAuthEntityList);
        if (null == $hasCheckEntity) {
            return;
        }

        if (!$this->entityAuthUtil->getEntityAuthUser($this->getUser(), $entity, $hasCheckEntity['field'])) {
            $this->catchAuthorizedEntity();
        }
    }

    private function catchAuthorizedEntity()
    {
        $this->flashBag->set("error", "authentication warning");
        throw new AuthenticationCredentialsNotFoundException("authentication warning");
    }

    /**
     * @return \Stringable|\Symfony\Component\Security\Core\User\UserInterface|null
     */
    public function getUser()
    {
        return $this->user;
    }
}
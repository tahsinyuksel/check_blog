<?php


namespace App\Controller;


use App\Model\CreateOrUpdateModel;
use App\Service\BaseEntityService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class BaseController extends AbstractController
{
    /**
     * @var BaseEntityService
     */
    protected $entityService;

    /**
     * @var string
     */
    protected $entityName;

    /**
     * @var string
     */
    protected $createFormType;

    /**
     * @var bool
     */
    protected $softDelete = false;

    /**
     * BaseController constructor.
     * @param BaseEntityService $entityService
     * @param string $entityName
     * @param string $createFormType
     * @param bool $softDelete
     */
    public function __construct(BaseEntityService $entityService, string $entityName, string $createFormType, bool $softDelete)
    {
        $this->entityService = $entityService;
        $this->entityName = $entityName;
        $this->createFormType = $createFormType;
        $this->softDelete = $softDelete;
    }

    public function formSave(
        FormInterface $form,
        Request $request,
        $successMessage = '',
        $errorMessage = ''
    ): CreateOrUpdateModel
    {
        $result = new CreateOrUpdateModel();

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $entity = $form->getData();
                $entity = $this->handleFormEntity($entity, $request);
                $result = $this->customResultAction($this->getEntityService()->save($entity), $successMessage, $errorMessage);
            }
            $result->setIsProcess(true);
        }

        return $result;
    }

    public function handleFormEntity($formEntity, Request $request)
    {
        return $formEntity;
    }

    public function handleAuthEntity($name, &$formEntity, Request $request = null)
    {

    }

    /**
     * @throws \Exception
     */
    public function getEntityForRequest(int $id)
    {
        if (0 === $id) {
            $entity = $this->getEntityService()->create();
        } else {
            $entity = $this->getEntityService()->getRepository()->find($id);
            if (!$entity) {
                throw $this->createNotFoundException(
                    'Entity not found with the following id: ' . $id
                );
            }
        }
        return $entity;
    }

    /**
     * @param bool|null $success
     * @param string $successMessage
     * @param string $errorMessage
     * @return CreateOrUpdateModel
     */
    public function customResultAction(?bool $success, string $successMessage = '', string $errorMessage = '') : CreateOrUpdateModel
    {
        $result = new CreateOrUpdateModel();
        if (null === $success) {
            return $result;
        }

        $result->setSuccessMessage($successMessage);
        $result->setErrorMessage($errorMessage);

        if ($success) {
            $result->setSuccess(true);
            $this->addFlash('success', $successMessage);
        } else {
            $this->addFlash('error', $errorMessage);
        }

        return $result;
    }

    public function getCrudLinks($name, $id = 0): array
    {
        $links = [
            'deleteLink' => $this->redirectToRoute(sprintf('%s_delete', $name), ['id'=> $id])->getTargetUrl(),
            'editLink' => $this->redirectToRoute(sprintf('%s_edit', $name), ['id'=> $id])->getTargetUrl(),
            'detailLink' => $this->redirectToRoute(sprintf('%s_detail', $name), ['id'=> $id])->getTargetUrl(),
            'indexLink' => $this->redirectToRoute(sprintf('%s_index', $name), [])->getTargetUrl(),
            'createLink' => $this->redirectToRoute(sprintf('%s_create', $name), [])->getTargetUrl(),
        ];

        if ($id == 0) {
            $links['editLink'] = '';
            $links['deleteLink'] = '';
            $links['detailLink'] = '';
        }

        return $links;
    }

    public function crudView(string $viewName, array $parameters = [], int $entityId = 0) : Response
    {
        $links = $this->getCrudLinks($this->getEntityName(), $entityId);
        $extraParams = [
            'entityName'=> $this->getEntityName()
        ];

        return $this->render(sprintf("%s/%s.html.twig", $this->getEntityName(), $viewName), array_merge(
            $parameters,
            $links,
            $extraParams
        ));
    }

    /**
     * @return BaseEntityService
     */
    public function getEntityService(): BaseEntityService
    {
        return $this->entityService;
    }

    /**
     * @return string
     */
    public function getEntityName(): string
    {
        return $this->entityName;
    }

    /**
     * @return string
     */
    public function getCreateFormType(): string
    {
        return $this->createFormType;
    }

    /**
     * @return bool
     */
    public function isSoftDelete(): bool
    {
        return $this->softDelete;
    }
}